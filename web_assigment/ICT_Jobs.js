var tableContent = [
    {
        "role": "Software Developers & Programmers",
        "Jobs on seek.co.nz 08/2018": "590",
        "Salary Max": "100",
        "Salary Min": "72",
        "Skills and Knowledge": ["computer software and systems", "programming languages and techniques", "software development processes such as Agile", "confidentiality", "data security and data protection issues"],
        "Job Description": "Software developers and programmers develop and maintain computer software, websites and software applications (apps)."
    },

    {
        "role": "Database & Systems Administrators",
        "Jobs on seek.co.nz 08/2018": "74",
        "Salary Max": "90",
        "Skills and Knowledge": ["a range of database technologies and operating systems", "new developments in databases and security systems", "computer and database principles and protocols"],
        "Job Description": "Database & systems administrators develop, maintain and administer computer operating systems, database management systems, and security policies and procedures."
    },
    {
        "role": "Help Desk & IT Support",
        "Jobs on seek.co.nz 08/2018": "143",
        "Salary Max": "65",
        "Salary Min": "46",
        "Skills and Knowledge": ["computer hardware, software, networks and websites", "the latest developments in information technology"],
        "Job Desciprion": "Data analysts identify and communicate trends in data using statistics and specialised software to help organisations achieve their business aims."
    },
    {
        "role": "Data Analyst",
        "Jobs on seek.co.nz 08/2018": "270",
        "Salary Max": "128",
        "Salary Min": "69",
        "Skills and Knowledge": ["data analysis tools such as Excel, SQL, SAP and Oracle, SAS or R", " data analysis, mapping and modelling techniques", "analytical techniques such as data mining"],
        "Job Description": "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems."
    },
    {
        "role": "Test Analyst",
        "Jobs on seek.co.nz 08/2018": "127",
        "Salary Max": "98",
        "Salary Min": "70",
        "Skills and Knowledge": ["programming methods and technology", "computer software and systems", " project management"],
        "Job Description": "Test analysts design and carry out testing processes for new and upgraded computer software and systems, analyse the results, and identify and report problems."
    },
    {
        "role": "Project Management",
        "Jobs on seek.co.nz 08/2018": "188",
        "Slsary Max": "190",
        "Salary Min": "110",
        "Skills and Knowledge": ["principles of project management", "approaches and techniques such as Kanban and continuous testing", "how to handle software development issues", " common web technologies used by the scrum team"],
        "Job Description": "Project managers use various methods to keep project teams on track. They also help remove obstacles to progress."
    }

]